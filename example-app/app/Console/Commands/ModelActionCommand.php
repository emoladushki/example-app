<?php

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;

class ModelActionCommand extends Command
{
    protected $signature = 'call-model-action {--model=} {--action=} {--data=} {--filter=}';

    public function handle(Controller $controller)
    {
        $controller->callingModelAction(
            $this->option('model'),
            $this->option('action'),
            $this->option('data'),
            $this->option('filter'),
        );
    }
}

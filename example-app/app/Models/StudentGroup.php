<?php

namespace App\Models;

use Database\Factories\StudentGroupFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Модель учебных групп для студентов
 *
 * @property integer $id Идендификатор группы
 * @property integer $course_id Идендификатор курса, по которому проходит обучения группа
 * @property string $name Название учебной группы
 * @property string $created_at Дата и время создания сущности учебной группы
 * @property string $updated_at Дата и время последнего обновления сущности учебной группы
 */
class StudentGroup extends BaseModel
{
    use HasFactory;

    protected $table = 'StudentGroup';

    protected $fillable = [
        'course_id',
        'name',
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [
        //
    ];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [
        'course_id' => 'nullable|integer|exists:Course,id',
        'name' => 'required|string|max:100',
    ];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [
        'course_id' => 'nullable|integer|exists:Course,id',
        'name' => 'nullable|string|max:100',
    ];

    protected static function newFactory(): StudentGroupFactory
    {
        return StudentGroupFactory::new();
    }
}

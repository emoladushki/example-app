<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Модель для логирования создания и удаления сущностей студентов
 *
 * @property integer $id Идендификатор сущности лога
 * @property integer $comment Комментарий о создании/удалении сущности студента
 * @property integer $student_id Идендификатор сущности студента, который был создан/удален
 * @property string $created_at Дата и время создания сущности лога
 * @property string $updated_at Дата и время последнего обновления сущности лога
 */
class StudentRecordLog extends BaseModel
{
    use HasFactory;

    protected $table = 'StudentRecordLog';

    protected $fillable = [
        //
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [
        //
    ];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [
        //
    ];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [
        //
    ];

}

<?php

namespace App\Models;

use Database\Factories\CourseFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Модель таблицы учебных курсов
 *
 * @property integer $id Идендификатор учебного курса
 * @property string $name Название учебного курса
 * @property string $created_at Дата и время создания сущности учебного курса
 * @property string $updated_at Дата и время последнего обновления сущности учебного курса
 */
class Course extends BaseModel
{
    use HasFactory;

    protected $table = 'Course';

    protected $fillable = [
        'name',
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [
        //
    ];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [
        'name' => 'required|string|max:100',
    ];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [
        'name' => 'nullable|string|max:100',
    ];

    protected static function newFactory(): CourseFactory
    {
        return CourseFactory::new();
    }
}

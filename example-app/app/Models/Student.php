<?php

namespace App\Models;

use Database\Factories\StudentFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Модель таблицы студентов
 *
 * @property integer $id Идендификатор студента
 * @property integer $student_group_id Идендификатор учебной группы, за которой закреплен студент
 * @property string $first_name Фамилия студента
 * @property string $second_name Имя студента
 * @property string $photo Фотография студента закодированая в base64
 * @property string $created_at Дата и время создания сущности студента
 * @property string $updated_at Дата и время последнего обновления сущности студента
 */
class Student extends BaseModel
{
    use HasFactory;

    protected $table = 'Student';

    protected $fillable = [
        'student_group_id',
        'first_name',
        'second_name',
        'photo',
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [
        //
    ];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [
        'student_group_id' => 'nullable|integer|exists:StudentGroup,id',
        'first_name' => 'required|string|max:100',
        'second_name' => 'required|string|max:100',
        'photo' => 'nullable|string|max:1000000',
    ];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [
        'student_group_id' => 'nullable|integer|exists:StudentGroup,id',
        'first_name' => 'nullable|string|max:100',
        'second_name' => 'nullable|string|max:100',
        'photo' => 'nullable|string|max:1000000',
    ];

    protected static function newFactory(): StudentFactory
    {
        return StudentFactory::new();
    }
}

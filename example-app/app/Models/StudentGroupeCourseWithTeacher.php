<?php

namespace App\Models;

use Database\Factories\StudentGroupeCourseWithTeacherFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Модель для создания связи между сущностями учителей и учебных групп
 *
 * @property integer $id Идендификатор сущности связи
 * @property integer $course_id Идендификатор курса, к которому назначен учитель
 * @property integer $teacher_id Идендификатор учителя, который ведет курс
 * @property integer $course_duration Продолжительность курса в секундах
 * @property string $status Статус согласования прикрепления курса к учителю
 * @property string $created_at Дата и время создания сущности связи
 * @property string $updated_at Дата и время последнего обновления сущности связи
 */
class StudentGroupeCourseWithTeacher extends BaseModel
{
    use HasFactory;

    const STATUS_TO_BE_AGREED = 'Ожидает согласования.';
    const STATUS_AGREED = 'Согласовано.';
    const STATUS_REJECTED = 'Отклонено.';

    const ALL_ALLOWED_STATUSES = [
        self::STATUS_TO_BE_AGREED,
        self::STATUS_AGREED,
        self::STATUS_REJECTED,
    ];

    protected $table = 'StudentGroupeCourseWithTeacher';

    protected $fillable = [
        'course_id',
        'teacher_id',
        'course_duration',
        'status',
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [
        'course_duration',
    ];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [
        'course_id' => 'required|integer|exists:Course,id',
        'teacher_id' => 'required|integer|exists:Teacher,id',
        'course_duration' => 'required|integer|max:1000000',
        'status' => 'required|string|in:Ожидает согласования., Согласовано., Отклонено.',
    ];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [
        'course_id' => 'nullable|integer|exists:Course,id',
        'teacher_id' => 'nullable|integer|exists:Teacher,id',
        'course_duration' => 'nullable|integer|max:1000000',
        'status' => 'nullable|string|in:Ожидает согласования.,Согласовано.,Отклонено.',
    ];

    protected static function newFactory(): StudentGroupeCourseWithTeacherFactory
    {
        return StudentGroupeCourseWithTeacherFactory::new();
    }
}

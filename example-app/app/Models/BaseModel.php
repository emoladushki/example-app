<?php

namespace App\Models;

use App\Helpers\ResponseHelper;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

abstract class BaseModel extends Model
{
    const ALL_FILTER_OPERATORS = [
        '>=',
        '=',
        '<=',
        '!=',
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [];

    /**
     * Получение массива со всеми существующими идендификаторами сущностей
     */
    public static function getExistsId(): array
    {
        return self::all()->pluck('id')->all();
    }

    /**
     * Получить объект сущности по идентификатору
     *
     * @param int|null $id
     *
     * @return Model
     *
     * @throws Exception
     */
    protected function getEntity(int $id = null): Model
    {
        try {
            $data = self::query()->findOrFail($id);
        } catch (\Exception $e) {
            throw new Exception('Unprocessable Entity', 422);
        }

        return $data;
    }

    /**
     * Отфильтрвать данные полученные с базы данных с заданными параметрами фильтрации
     *
     * @param Collection $data
     * @param array $filters
     *
     * @return Collection
     */
    private function dataFilter(Collection $data, array $filters): Collection
    {
        foreach ($filters as $filter) {
            $data = $data->where($filter[0], $filter[1], $filter[2]);
        }

        return $data->values();
    }

    /**
     * Метод для получения массива сущностей из базы данных
     *
     * @param array|null $filters
     *
     * @return void
     */
    protected function getItems(array $filters = null): void
    {
        $data = self::all();

        if (!empty($filters)) {
            $data = $this->dataFilter($data, $filters);
        }

        ResponseHelper::consoleSuccess($data->toArray(), 'Success', 200);
    }

    /**
     * Метод для получения массива сущностей из базы данных
     *
     * @param int|null $id
     *
     * @return void
     *
     * @throws Exception
     */
    protected function getItem(int $id = null): void
    {
        $data = $this->getEntity($id)->toArray();

        ResponseHelper::consoleSuccess($data, 'Success', 200);
    }

    /**
     * Метод для создания новой сущности в базе данных
     */
    protected function createItem(array $data): void
    {
        $validateData = Validator::validate($data, $this->rulesCreateValidation);

        $data = self::query()->create($validateData)->toArray();

        ResponseHelper::consoleSuccess($data, 'Created', 201);
    }

    /**
     * Метод для создания новой сущности в базе данных
     *
     * @throws Exception
     */
    protected function updateItem(array $data): void
    {
        $validateData = Validator::validate($data, $this->rulesUpdateValidation);

        $data = $this->getEntity($data['id'] ?? null);

        $data->update($validateData);

        $data = array_merge($data->toArray(), $validateData);

        ResponseHelper::consoleSuccess($data, 'Updated', 200);
    }

    /**
     * Метод для удаления сущности из базы данных
     *
     * @param int|null $id
     *
     * @throws Exception
     */
    protected function deleteItem(int $id = null): void
    {
        $this->getEntity($id)->delete();

        ResponseHelper::consoleSuccess(null, 'Deleted', 200);
    }

    /**
     * Метод используемый пользователем как действие, для получения всех сущностей одной таблицы, с возможностью фильтрации
     *
     * @param array|null $data
     * @param array|null $filters
     */
    public function actionGetRecords(array $data = null, array $filters = null): void
    {
        $this->getItems($filters);
    }

    /**
     * Метод используемый пользователем как действие, для получения одной сущности по её идентификатору
     *
     * @param array|null $data
     * @param array|null $filters
     *
     * @throws Exception
     */
    public function actionGetRecord(array $data = null, array $filters = null): void
    {
        $this->getItem($data['id'] ?? null);
    }

    /**
     * Метод используемый пользователем как действие, для создание новой сущности в базе данных
     *
     * @param array|null $data
     * @param array|null $filters
     */
    public function actionCreateRecord(array $data = null, array $filters = null): void
    {
        $this->createItem($data);
    }

    /**
     * Метод используемый пользователем как действие, для создание новой сущности в базе данных
     *
     * @param array|null $data
     * @param array|null $filters
     *
     * @throws Exception
     */
    public function actionUpdateRecord(array $data = null, array $filters = null): void
    {
        $this->updateItem($data);
    }

    /**
     * Метод используемый пользователем как действие, для удаления сущности из базы данных
     *
     * @param array|null $data
     * @param array|null $filters
     *
     * @throws Exception
     */
    public function actionDeleteRecord(array $data = null, array $filters = null): void
    {
        $this->deleteItem($data['id'] ?? null);
    }
}

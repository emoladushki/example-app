<?php

namespace App\Models;

use Database\Factories\TeacherFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Модель таблицы учителей
 *
 * @property integer $id Идендификатор учителя
 * @property string $name Имя учителя
 * @property string $created_at Дата и время создания сущности учителя
 * @property string $updated_at Дата и время последнего обновления сущности учителя
 */
class Teacher extends BaseModel
{
    use HasFactory;

    protected $table = 'Teacher';

    protected $fillable = [
        'name',
    ];

    /**
     * Массив с названиями колонок, которым разрешена фильтрации
     *
     * @var array
     */
    public $arrayColumnsAllowedToFilter = [
        //
    ];

    /**
     * Массив с правилами валидации для сохранения сущности в базу данных
     *
     * @var array
     */
    protected $rulesCreateValidation = [
        'name' => 'required|string|max:100',
    ];

    /**
     * Массив с правилами валидации для обновления данных сущности в базе данных
     *
     * @var array
     */
    protected $rulesUpdateValidation = [
        'name' => 'nullable|string|max:100',
    ];


    protected static function newFactory(): TeacherFactory
    {
        return TeacherFactory::new();
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models;
use App\Models\BaseModel;
use Exception;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    public $modelList = [
        'course' => Models\Course::class,
        'student' => Models\Student::class,
        'student-group' => Models\StudentGroup::class,
        'student-groupe-course-with-teacher' => Models\StudentGroupeCourseWithTeacher::class,
        'student-record-log' => Models\StudentRecordLog::class,
        'teacher' => Models\Teacher::class,
    ];

    /**
     * Вызов действия из модели
     *
     * @param string $modelName
     * @param string $action
     * @param string|null $data
     * @param string|null $filter
     */
    public function callingModelAction(string $modelName, string $action, string $data = null, string $filter = null): void
    {
        $model = $this->getModel($modelName);

        $action = $this->checkActionAndConvertToLowerCamelCase($action, $model);

        $data = $this->convertDataToAssociativeArray($data);

        $filtersArray = $this->checkFilterAndConvertToAssociativeArray($model, $filter);

        try {
            $model->$action($data, $filtersArray);
        } catch (ValidationException $exception) {
            ResponseHelper::consoleError(
                $exception->validator->getMessageBag(),
                $exception->status
            );
        } catch (Exception $exception) {
            ResponseHelper::consoleError(
                $exception->getMessage(),
                $exception->getCode()
            );
        }
    }

    /**
     * Получить объект модели по строке
     *
     * @param string $modelName
     *
     * @return BaseModel
     */
    protected function getModel(string $modelName): BaseModel
    {
        try {
            return new $this->modelList[$modelName];
        } catch (Exception $exception) {
            ResponseHelper::consoleError('model: Model not found', 400);
        }
    }

    /**
     * Конвертировать действие в lower camel case и проверить на существование в модели
     *
     * @param string $action
     * @param BaseModel $model
     *
     * @return string
     */
    protected function checkActionAndConvertToLowerCamelCase(string $action, BaseModel $model): string
    {
        $action = 'action' . str_replace(' ', '', ucwords(str_replace('-', ' ', $action)));

        if (!method_exists($model, $action)) {
            ResponseHelper::consoleError('action: Action not found', 400);
        }

        return $action;
    }

    /**
     * Конвертировать строку с данными в ассоциативный массив
     *
     * @param string|null $data
     *
     * @return array|null
     */
    protected function convertDataToAssociativeArray(string $data = null): ?array
    {
        if (empty($data)) {
            return null;
        }

        try {
            $dataArray = explode('/', $data);

            $associativeDataArray = [];
            foreach ($dataArray as $oneData) {
                $oneDataArray = explode('=', $oneData);
                $associativeDataArray[$oneDataArray[0]] = $oneDataArray[1];
            }

            return $associativeDataArray;
        } catch (Exception $exception) {
            ResponseHelper::consoleError('data: Incorrect data format', 400);
        }
    }

    /**
     * Конвертировать строку с параметрами фильтрации в ассоциативный массив и провалидировать
     *
     * @param BaseModel $model
     * @param string|null $filter
     *
     * @return array|null
     */
    protected function checkFilterAndConvertToAssociativeArray(BaseModel $model, string $filter = null): ?array
    {
        if (empty($filter)) {
            return null;
        }

        try {
            $filterArray = explode('.', $filter);

            $associativeFiltersArray = [];
            foreach ($filterArray as $oneFilter) {
                $oneFilterArray = explode(',', $oneFilter);

                $this->validateFilterParam($model, $oneFilterArray);

                $associativeFiltersArray[] = [$oneFilterArray[0], $oneFilterArray[1], $oneFilterArray[2]];
            }

            return $associativeFiltersArray;
        } catch (Exception $exception) {
            ResponseHelper::consoleError('filter: Incorrect filter format', 400);
        }
    }

    /**
     * Провалидировать параметры фильтрации данных
     *
     * @throws Exception
     */
    protected function validateFilterParam(BaseModel $model, array $oneFilterArray): void
    {
        if (!in_array($oneFilterArray[0], $model->arrayColumnsAllowedToFilter)
            || !in_array($oneFilterArray[1], $model::ALL_FILTER_OPERATORS)) {
            throw new Exception();
        }
    }
}

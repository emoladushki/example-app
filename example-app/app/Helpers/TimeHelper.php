<?php

namespace App\Helpers;

class TimeHelper
{
    /**
     * 12 часов в секундах
     */
    const TWELVE_HOURS_IN_SECONDS = 43200;

    /**
     * 24 часа в секундах
     */
    const TWENTY_FOUR_HOURS_IN_SECONDS = 86400;

    /**
     * 36 часов в секундах
     */
    const THIRTY_SIX_HOURS_IN_SECONDS = 129600;

    /**
     * 48 часов в секундах
     */
    const FORTY_EIGHT_HOURS_IN_SECONDS = 172800;
}

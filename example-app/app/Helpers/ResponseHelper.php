<?php


namespace App\Helpers;


class ResponseHelper
{
    /**
     * Возвращение позитивного ответа в консоли
     *
     * @param array|null $data Данные отображаемые в консоли
     * @param string|null $message Сообщение отображаемое в кончоли
     * @param int $code Http код ответа
     */
    public static function consoleSuccess(array $data = null, string $message = null, int $code = 200): void
    {
        var_dump([
            'message' => $message,
            'data' => $data,
            'code' => $code
        ]);

        exit();
    }

    /**
     * Возвращение негативного ответа в консоли
     *
     * @param int $code Http код ошибки
     */
    public static function consoleError(string $message, int $code = 500): void
    {
        var_dump([
            'message' => $message,
            'data' => null,
            'code' => $code
        ]);

        exit();
    }
}

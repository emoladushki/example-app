<?php

namespace Database\Seeders;

use App\Models\StudentGroupeCourseWithTeacher;
use Illuminate\Database\Seeder;

class StudentGroupeCourseWithTeacherSeeder extends Seeder
{
    const COUNT_OF_ENTITIES = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudentGroupeCourseWithTeacher::factory()
        ->count(self::COUNT_OF_ENTITIES)
        ->create();
    }
}

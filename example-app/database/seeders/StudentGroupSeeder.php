<?php

namespace Database\Seeders;

use App\Models\StudentGroup;
use Illuminate\Database\Seeder;

class StudentGroupSeeder extends Seeder
{
    const COUNT_OF_ENTITIES = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudentGroup::factory()
        ->count(self::COUNT_OF_ENTITIES)
        ->create();
    }
}

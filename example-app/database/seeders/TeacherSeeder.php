<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    const COUNT_OF_ENTITIES = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teacher::factory()
        ->count(self::COUNT_OF_ENTITIES)
        ->create();
    }
}

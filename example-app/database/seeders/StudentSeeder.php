<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    const COUNT_OF_ENTITIES = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::factory()
        ->count(self::COUNT_OF_ENTITIES)
        ->create();
    }
}

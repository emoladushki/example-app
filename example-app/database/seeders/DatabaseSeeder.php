<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TeacherSeeder::class,
            CourseSeeder::class,
            StudentGroupeCourseWithTeacherSeeder::class,
            StudentGroupSeeder::class,
            StudentSeeder::class,
        ]);
    }
}

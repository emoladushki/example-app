<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    const COUNT_OF_ENTITIES = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::factory()
            ->count(self::COUNT_OF_ENTITIES)
            ->create();
    }
}

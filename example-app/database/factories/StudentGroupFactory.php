<?php

namespace Database\Factories;

use App\Models\Course;
use App\Models\StudentGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StudentGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'course_id' => $this->faker->randomElement(Course::getExistsId()),
            'name' => $this->faker->unique()->numerify('Test group ####'),
        ];
    }
}

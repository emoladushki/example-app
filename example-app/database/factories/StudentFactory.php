<?php

namespace Database\Factories;

use App\Models\Student;
use App\Models\StudentGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    const MAX_LENGTH_STUDENT_FIRST_NAME = 10;
    const MAX_LENGTH_STUDENT_SECOND_NAME = 10;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->unique()->text(self::MAX_LENGTH_STUDENT_FIRST_NAME),
            'second_name' => $this->faker->unique()->text(self::MAX_LENGTH_STUDENT_SECOND_NAME),
            'photo' => null,
            'student_group_id' => $this->faker->randomElement(StudentGroup::getExistsId()),
        ];
    }
}

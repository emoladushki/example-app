<?php

namespace Database\Factories;

use App\Helpers\TimeHelper;
use App\Models\Course;
use App\Models\StudentGroupeCourseWithTeacher;
use App\Models\Teacher;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentGroupeCourseWithTeacherFactory extends Factory
{
    /**
     * Массив длительности курсов для генерации тестовых данных
     */
    const COURSE_DURATION_FOR_DATA_GENERATION = [
        TimeHelper::TWELVE_HOURS_IN_SECONDS,
        TimeHelper::TWENTY_FOUR_HOURS_IN_SECONDS,
        TimeHelper::THIRTY_SIX_HOURS_IN_SECONDS,
        TimeHelper::FORTY_EIGHT_HOURS_IN_SECONDS,
    ];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StudentGroupeCourseWithTeacher::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'course_id' => $this->faker->randomElement(Course::getExistsId()),
            'teacher_id' => $this->faker->randomElement(Teacher::getExistsId()),
            'course_duration' => $this->faker->randomElement(self::COURSE_DURATION_FOR_DATA_GENERATION),
            'status' => $this->faker->randomElement(StudentGroupeCourseWithTeacher::ALL_ALLOWED_STATUSES),
        ];
    }
}

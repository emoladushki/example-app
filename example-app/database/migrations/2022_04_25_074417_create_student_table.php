<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Student', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('student_group_id')->unsigned()->nullable();
            $table->string('first_name');
            $table->string('second_name');
            $table->longText('photo')->nullable();
            $table->timestamps();

            $table->foreign('student_group_id')->references('id')->on('StudentGroup')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Student');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentGroupeCourseWithTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StudentGroupeCourseWithTeacher', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('teacher_id')->unsigned();
            $table->integer('course_duration')->unsigned();
            $table->string('status')->unsigned();
            $table->timestamps();

            $table->foreign('course_id')->references('id')->on('Course')->cascadeOnDelete();
            $table->foreign('teacher_id')->references('id')->on('Teacher')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('StudentGroupeCourseWithTeacher');
    }
}

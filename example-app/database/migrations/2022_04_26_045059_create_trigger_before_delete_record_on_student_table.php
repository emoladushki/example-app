<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTriggerBeforeDeleteRecordOnStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $functionAfterCreateStudentRecord = <<<SQL
        CREATE OR REPLACE FUNCTION function_create_log_before_delete_student()
        RETURNS TRIGGER AS $$
        BEGIN
          INSERT INTO "StudentRecordLog"(
            student_id, comment, created_at, updated_at)
            VALUES (old.id, 'Deleted', NOW(), NOW());

          RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
SQL;

        $triggerAfterCreateStudentRecord = <<<SQL
        CREATE TRIGGER trigger_create_log_before_delete_student
          BEFORE DELETE ON "Student"
          FOR EACH ROW
          EXECUTE PROCEDURE function_create_log_before_delete_student()
SQL;


        DB::unprepared($functionAfterCreateStudentRecord);
        DB::unprepared($triggerAfterCreateStudentRecord);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(<<<SQL
        DROP TRIGGER IF EXISTS trigger_create_log_before_delete_student on "Student"
SQL);
        DB::unprepared(<<<SQL
        DROP FUNCTION function_create_log_before_delete_student;
SQL);
    }
}
